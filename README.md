## Iniciando
```sh
composer install
touch database.sqlite
cp .env.example .env
php artisan passport:install
php artisan key:generate

```
As configurações de conexão vão diretamente no arquivo `.env` da raiz do projeto:

```yaml
DB_CONNECTION=sqlite
DB_DATABASE=/absolute/path/to/database.sqlite

# DB_CONNECTION=mysql
# DB_HOST=127.0.0.1
# DB_PORT=3306
# DB_DATABASE=laravel
# DB_USERNAME=root
# DB_PASSWORD=
```

E agora, para começar a gerenciar o banco, podemos utilizar o comando

```
php artisan migrate --seed
```

Então servimos com

```
php artisan serve
```