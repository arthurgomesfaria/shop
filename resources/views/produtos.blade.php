@extends('layouts.app')

@section('content')
<div class="">
    <div class="row justify-content-center" style='width: 100%'>
        <div class="col-8">
            <div class="card">
                <div class="card-header">Produtos</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class='row mx-lg-n5'>
                        @foreach ($produtos as $produto)
                        <div class='col-6' style='padding: 10px'>
                            <div class="card" style="width: 100%; height: 100%">
                                <div class="card-body">
                                    <h5 class="card-title">{{$produto->title}}</h5>
                                    <p class="card-text">{{$produto->short_description}}</p>
                                    <p class="card-text">
                                    Preço Original: {{$produto->price}} <br> Desconto de {{$produto->discount}}%
                                    </p>
                                    <a type="button" class="btn btn-success" href='/comprar/{{$produto->id}}'>Comprar por R${{$produto->precoComDesconto()}}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
