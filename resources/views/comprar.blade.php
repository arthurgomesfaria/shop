@extends('layouts.app')

@section('content')

<div class="criar-produto-page">
  <div class="form">
    <form class="login-form" method="POST" action="/comprar">
      @csrf
      <input id="title" readonly type="text" class="form-control" name="title" placeholder="Nome do produto" required  value="{{$produto->title}}">
      <input id="produtoId" readonly type="text" class="form-control" name="produtoId" placeholder="Nome do produto" required  value="{{$produto->id}}">
      <input id="quantity" type="number" class="form-control" name="quantity" placeholder="Quantidade" autofocus required>
    <br>
      <button type='submit' id='submit'>Comprar</button>
    </form>
  </div>
</div>
@endsection
