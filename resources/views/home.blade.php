@extends('layouts.app')

@section('content')
<div class="">
    <div class="row justify-content-center" style='width: 100%'>
        <div class="col-8">
            <div class="card">
                <div class="card-header">Pedidos</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h6>Meus Pedidos</h6>
                    <div class='row mx-lg-n5'>
                        @foreach ($pedidos as $pedido)
                        <div class='col-6' style='padding: 10px'>
                            <div class="card" style="width: 100%; height: 100%">
                                <div class="card-body">
                                    <h5 class="card-title">Pedido #{{$pedido->id}}</h5>
                                    <p class="card-text">Feito em {{$pedido->created_at}}</p>
                                    <p class="card-text">
                                    <ul class="list-group">
                                    @foreach ($pedido->produtos as $produto)
                                        <li class="list-group-item">{{$produto->title}} * {{$produto->pivot->quantity}} = {{$produto->pivot->quantity * $produto->price}}</li>
                                    </ul>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
