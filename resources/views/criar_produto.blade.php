@extends('layouts.app')

@section('content')

<div class="criar-produto-page">
  <div class="form">
    <form class="login-form" method="POST" action="/criar-produto">
      @csrf
      <input id="title" type="text" class="form-control" name="title" placeholder="Nome do produto" required autofocus>
      <input id="short_description" type="text" class="form-control" name="short_description" placeholder="Pequena Descrição" required>
      <input id="long_description" type="text" class="form-control" name="long_description" placeholder="Descrição Completa" required>
      <input id="price" type="number" class="form-control" name="price" placeholder="Preço em Reais" required>
      <input id="discount" type="number" class="form-control" name="discount" placeholder="Desconto em %" required>
      <input id="inventory" type="number" class="form-control" name="inventory" value="1000" placeholder="Quantidade em Estoque" required>
    <br>
      <button type='submit' id='submit'>Criar</button>
    </form>
  </div>
</div>
@endsection
