<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
Route::get('/', function () {
    return view('welcome');
});
Route::get('/criar-produto', function () {
    return view('criar_produto');
})->middleware('auth');

Route::get('/produtos', function () {
    return view('produtos', ['produtos' => App\Produto::all()]);
});

Route::get('/comprar/{id}', function ($id) {
    return view('comprar', ['produto' => App\Produto::find($id)]);
})->middleware('auth');

Route::post('/comprar', function (Request $request) {
    $input = $request->all();
    $pedido = new App\Pedido;
    $pedido->status = 0;
    $pedido->user_id = Auth::id();
    $pedido->save();
    $pedido->produtos()->sync([$input['produtoId'] => ['quantity' => $input['quantity']]]);
    $pedido->save();
    return redirect('/home');
    return redirect('/home');
})->middleware('auth');

Route::post('/criar-produto', function (Request $request) {
    $input = $request->all();
    $product = App\Produto::create($input);
    return redirect('/produtos');
})->middleware('auth');

Auth::routes();

Route::get('/home', function () {
    $pedidos = App\Pedido::where('user_id', '=', Auth::id())->with('produtos')->get();
    return view('home', ['pedidos' => $pedidos]);
})->middleware('auth');

Auth::routes(['verify' => true]);
